# Intialisation J5

dans le cadre d'un mise en commun entre J5 et Express vous pouvez utilisez le dépôt pour tester le fonctionnement de J5

Il faut brancher un ARDUINO UNO sur votre poste pour pouvoir tester.

Dupliquer ce dépôt sur votre poste

```
git clone https://gitlab.com/oktopod/public/j5-demo
```

Installer les dépendances

```
npm install
```

démarrer le projet pour le tester

```
node app.js
```
