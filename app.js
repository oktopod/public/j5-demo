const express = require('express')
var five = require("johnny-five")
const app = express()
const port = 3000

var board = new five.Board("COM3");

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res) {
    res.send('API J5');
});

app.get('/led/on', function (req, res) {
    var led = new five.Led(13);
    led.on();
    res.send('LED ON');
});

app.get('/led/off', function (req, res) {
    res.send('LED OFF');
    var led = new five.Led(13);
    led.off();
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
